package fuctions

import "fmt"
import "database/sql"
import _ "github.com/go-sql-driver/mysql"
import "encoding/json"

func GetBaca(data string) string {	
	return data
}

func connect() (*sql.DB, error) {
    db, err := sql.Open("mysql", "root:1234567890@tcp(127.0.0.1:3306)/majoo_user")
    if err != nil {
        return nil, err
    }

    return db, nil
}

func QueryRegisUser(id, userName, pass, nama, imageName string) string{

	db, err := connect()
    if err != nil {
        fmt.Println(err.Error())
        return err.Error()
    }
    defer db.Close()

    _, err = db.Exec("insert into users values (?, ?, ?, ?, ?)", id, userName, pass, nama, imageName)
    if err != nil {
        fmt.Println(err.Error())
        return err.Error()
    }
	return "insert success!"
}

func QueryEditUser(id, userName, pass, nama, imageName string) string{

	db, err := connect()
    if err != nil {
        fmt.Println(err.Error())
        return err.Error()
    }
	defer db.Close()
	
	_, err = db.Exec("update users set username = ?, password = ?, namaLengkap = ?, imageName = ? where id = ?", userName, pass, nama, imageName, id)
    if err != nil {
        fmt.Println(err.Error())
        return err.Error()
    }

	return "update success!"
}

type userLogin struct {
	UserName string `json:"username"`
	Pass string `json:"password"`
}

func QueryLoginUser(id string) string{

	var db, err = connect()
    if err != nil {
        fmt.Println(err.Error())
    }
	defer db.Close()

		rows, err := db.Query("select username, password from users where id = ?", id)
		if err != nil {
			fmt.Println(err.Error())
			return err.Error()
		}
		defer rows.Close()
	
		var result []userLogin
	
		for rows.Next() {
			var each = userLogin{}
			var err = rows.Scan(&each.UserName, &each.Pass)
	
			if err != nil {
				fmt.Println(err.Error())
				return err.Error()
			}
	
			result = append(result, each)
		}
	
		if err = rows.Err(); err != nil {
			fmt.Println(err.Error())
			return err.Error()
		}
		var jsonResult, err1 = json.Marshal(result)
		if err1 != nil {
			fmt.Println(err1.Error())
			return err1.Error()
		}
	
		var jsonResultFinal = string(jsonResult)
		fmt.Println(jsonResultFinal)
		return jsonResultFinal
}

type userData struct {
	id string 
	username string
	password string
	namaLengkap string
	imageName string
}

type dataResp struct {
	Id string `json:"id"`
	UserName string `json:"username"`
	Pass string `json:"password"`
	NamaLengkap string `json:"namaLengkap"`
	ImageName string `json:"imageName"`
}

func QueryShowAllsUser() string{

	db, err := connect()
    if err != nil {
        fmt.Println(err.Error())
        return err.Error()
    }
    defer db.Close()

    rows, err := db.Query("select * from users")
    if err != nil {
        fmt.Println(err.Error())
        return err.Error()
    }
    defer rows.Close()

	var result []dataResp

	for rows.Next() {
        var each = dataResp{}
        var err = rows.Scan(&each.Id, &each.UserName, &each.Pass, &each.NamaLengkap, &each.ImageName)

        if err != nil {
            fmt.Println(err.Error())
            return err.Error()
        }

        result = append(result, each)
    }

    if err = rows.Err(); err != nil {
        fmt.Println(err.Error())
        return err.Error()
	}
	var jsonResult, err1 = json.Marshal(result)
	if err1 != nil {
		fmt.Println(err1.Error())
		return err1.Error()
	}

	var jsonResultFinal = string(jsonResult)
	fmt.Println(jsonResultFinal)
	return jsonResultFinal
}
