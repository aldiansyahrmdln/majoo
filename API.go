package main

import "fmt"
import "encoding/json"
import "net/http"
import "Majoo/MyRespository/fuctions"

type requestRegis struct {
	Id string `json:"id"`
	UserName string `json:"username"`
	Pass string `json:"password"`
	NamaLengkap string `json:"namaLengkap"`
	ImageName string `json:"imageName"`
}

type response struct{
	Response string
}

func regisUser(w http.ResponseWriter, r *http.Request) {
    w.Header().Set("Content-Type", "application/json")

    if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		fmt.Println(decoder)
		var dataR requestRegis 
		errR := decoder.Decode(&dataR)
		if errR != nil {
			panic(errR)
		}
		fmt.Println(dataR.Id)

				var hasilQuery string
				hasilQuery = fuctions.QueryRegisUser(dataR.Id,dataR.UserName,dataR.Pass,dataR.NamaLengkap,dataR.ImageName)
				fmt.Println(hasilQuery)

				var resp = []response{{hasilQuery}}
				var respJson, err1 = json.Marshal(resp)
				if err1 != nil {
					fmt.Println(err1.Error())
					return
				}

				w.Write(respJson)        

        http.Error(w, "User not found", http.StatusBadRequest)
        return 
    }

    http.Error(w, "", http.StatusBadRequest)
}

func updateUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		fmt.Println(decoder)
		var dataR requestRegis 
		errR := decoder.Decode(&dataR)
		if errR != nil {
			panic(errR)
		}
		fmt.Println(dataR.Id)

				var hasilQuery string
				hasilQuery = fuctions.QueryEditUser(dataR.Id,dataR.UserName,dataR.Pass,dataR.NamaLengkap,dataR.ImageName)
				fmt.Println(hasilQuery)

				var resp = []response{{hasilQuery}}
				var respJson, err1 = json.Marshal(resp)
				if err1 != nil {
					fmt.Println(err1.Error())
					return
				}

				w.Write(respJson)        

        http.Error(w, "User not found", http.StatusBadRequest)
        return 
    }

    http.Error(w, "", http.StatusBadRequest)

}

type userLogin struct {
	UserName string `json:"username"`
	Pass string `json:"password"`
}

func loginUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		fmt.Println(decoder)
		var dataR requestRegis 
		errR := decoder.Decode(&dataR)
		if errR != nil {
			panic(errR)
		}
		fmt.Println(dataR.Id)

				var hasilQuery string
				hasilQuery = fuctions.QueryLoginUser(dataR.Id)
				// fmt.Println(hasilQuery)

				var data2 []userLogin
				var err2 = json.Unmarshal([]byte(hasilQuery), &data2)
				if err2 != nil {
					fmt.Println(err2.Error())
					return
				}
				fmt.Println(data2[0].UserName)
				fmt.Println(data2[0].Pass)
				
				var statusLogin string

				if dataR.UserName == data2[0].UserName {
					if dataR.Pass == data2[0].Pass {
						statusLogin = "success"
					}else{
						statusLogin = "wrong password"
					}
				}else{
					statusLogin = "wrong username"
				}
					


				var resp = []response{{statusLogin}}
				var respJson, err1 = json.Marshal(resp)
				if err1 != nil {
					fmt.Println(err1.Error())
					return
				}

				w.Write(respJson)        

        http.Error(w, "User not found", http.StatusBadRequest)
        return 
    }

    http.Error(w, "", http.StatusBadRequest)

}

func showAllUser(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

    if r.Method == "GET" {

		var hasilQuery string
			hasilQuery = fuctions.QueryShowAllsUser()
			fmt.Println("hasil query show all")
			// fmt.Println(hasilQuery)
			
			var data2 []requestRegis

			var err2 = json.Unmarshal([]byte(hasilQuery), &data2)
			if err2 != nil {
				fmt.Println(err2.Error())
				return
			}

			var respJson, err1 = json.Marshal(data2)
				if err1 != nil {
					fmt.Println(err1.Error())
					return
				}

		w.Write(respJson)        

	http.Error(w, "User not found", http.StatusBadRequest)
	return 

		
    }

    http.Error(w, "", http.StatusBadRequest)
}

func main() {

	http.HandleFunc("/registrasi/user", regisUser)
	http.HandleFunc("/show/all/user", showAllUser)
	http.HandleFunc("/update/user", updateUser)
	http.HandleFunc("/login/user", loginUser)

	fmt.Println("starting web server at http://localhost:8080/")
    http.ListenAndServe(":8080", nil)

}
